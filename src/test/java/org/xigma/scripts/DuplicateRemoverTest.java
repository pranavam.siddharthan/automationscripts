package org.xigma.scripts;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class DuplicateRemoverTest {

	@Test
	public void givenList_WhenRemoveDuplicates_ThenDuplicatesAreRemovedRetainingOne() {
		assertEquals(
				Arrays.asList("Hello", "hello", "world", "vanakkam", "chennai", "Vanakkam", "Madras", "WORLD",
						"hello world", "W orld"),
				new DuplicateRemover().removeDuplicates(Arrays.asList("Hello", "hello", "world", "vanakkam", "hello",
						"chennai", "Vanakkam", "Madras", "WORLD", "hello world", "W orld")));
	}

}
