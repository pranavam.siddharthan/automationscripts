package org.xigma.scripts;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class SorterTest {

	@Test
	public void givenUnSortedListWhenSortedThenToBeSorted() {
		assertEquals(Arrays.asList(Integer.MIN_VALUE, 10, 15, 20, 150, 20000, 150000, 250000, Integer.MAX_VALUE / 8,
				Integer.MAX_VALUE / 7, Integer.MAX_VALUE / 6, Integer.MAX_VALUE / 5, Integer.MAX_VALUE / 4,
				Integer.MAX_VALUE / 3, Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 1)
				,
				new Sorter().sort(Arrays.asList(Integer.MIN_VALUE, 10, 15, 20, 150, 20000, 150000, 250000,
						Integer.MAX_VALUE / 1, Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 3, Integer.MAX_VALUE / 4,
						Integer.MAX_VALUE / 5, Integer.MAX_VALUE / 6, Integer.MAX_VALUE / 7, Integer.MAX_VALUE / 8)));
	}

}
